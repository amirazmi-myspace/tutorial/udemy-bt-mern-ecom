import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { Product } from '../components/Product'
import { Col, Row } from 'react-bootstrap'

export const Home = () => {
  const [state, setState] = useState({ products: [] })
  const { products } = state

  useEffect(() => {
    const fetchProducts = async () => {
      const { data } = await axios.get('/api/products')
      setState((prev) => { return { ...prev, products: data } })
    }
    fetchProducts()
  }, [products])


  const renderContent = () => {
    return products.map(product => (
      <Col key={product._id} sm={12} md={6} lg={4} xl={3} >
        <Product product={product} />
      </Col >
    ))
  }

  return (
    <>
      <h1>Latest Products</h1>
      <Row>
        {renderContent()}
      </Row>
    </>
  )
}
